//===================================================================================================================
//===================================================================================================================

//�������, ����������� ������� �� ��� ������. ����, ���� ������, ��� � ����� ������� ��������� ����� � ������ ������
//����������, �� ��� ��������� ���������� ����-�����-��������. ��� �������� �������������, � �� ����� ��*���, ��� ���
//��������� ����� �������� ����� ��� ������ �� ������.

//13 ����, �������, 23-05

//===================================================================================================================
//===================================================================================================================

//������ ���������� � .Next ��������� ������ ����������. �������

//13 ����, �������, 23-09

//===================================================================================================================
//===================================================================================================================

//���������� ���� � �������

//13 ����, �������, 23-15

//===================================================================================================================
//===================================================================================================================

//������� ��������� ��� � �������� BiomeGeneration.CoverSurfaceWithSoils

//13 ����, �������, 23-39

//===================================================================================================================
//===================================================================================================================

//����� ������������ ������������� ������� � ���� � ������ ���������� �����, � ������� � ����� ����� �� �����,
//� ���� ��������� ��� �����������. ������ � ����, � ������� (�������!) ���������� ����. ��, ��� �� � ��� ���
//���������� ���. ���� � ����� ���� �������� ��������, ����� ���������� - � �� ������.

//���-��� ��������� ����� � ����� �� ����, ��� ���� XNA-����, � ���������, ��� �����-�� ������� � ��� ������,
//�� ������������ ������. � �� ��� �������, ��� ��� �� ������ ���������.

//��� ��� �� ���� ���������� � �������� ��� ��������� ��������. ���-�� �� ���.

//����� ���������� �������� TerrainGen �� ������ 0.1.1.0

//14 ����, ����������, 2-30

//===================================================================================================================
//===================================================================================================================

//����������. ��� �� �����������, �� ���. �������, ���� � Block Column.

//14 ����, �����������, 3-15

//===================================================================================================================
//===================================================================================================================

//��. ������������ ��� �� ��������� ���������� �������� (��������, SetAt (31, 90, 256, BlockType.Air)). �����������,
//��� ��������� ������� �� ����� ������ �� ����������. 

//14 ����, �����������, 3-20

//===================================================================================================================
//===================================================================================================================

//���� �� ������ BlockArray �� ��� ����������. ���� �� �����������...���� ���. ������ �����-��...

//14 ����, �����������, 3-31

//===================================================================================================================
//===================================================================================================================

//BlockArray, ������������ TerrainGenerator, ������ ����. ��������, ���� ��������� � ������������� ���������. �� ���?
//��� ����������, ������ ������ � ���������� ��������� �� ����.

//������ ����! ��� � ����� �� ���?! ������...
//�������, � ��������... � ����, ������� ��� �.... ��������?! ���. ��...!

//�� ������...

//14 ����, �����������, 3-54

//===================================================================================================================
//===================================================================================================================

//� ����� �����. ��������� �������������� ������ ����� TerrainGenerator �� ������������� ������ ������

//14 ����, �����������, 15-17

//===================================================================================================================
//===================================================================================================================

//���������� ������ �������. ��� ���������. ��� ����.

//14 ����, �����������, 18-36

//===================================================================================================================
//===================================================================================================================

//������� ��������� ���������, GenerateIsland ������ ���������� IslandBlockStorage.

//14 ����, �����������, 19-16

//===================================================================================================================
//===================================================================================================================

//������ ������� ABlueprints, ���������������� ����������� ������� � ��� ���������. � �� ������� �������
//��������� ��������� - ������� ���. ��� ������ ������ ����.

//14 ����, �����������, 22-28

//===================================================================================================================
//===================================================================================================================

//��������� � ABlueprints, ������� �������� ISL � ����� ��������� ���������.

//14 ����, �����������, 22-53

//===================================================================================================================
//===================================================================================================================

//���������� �������� ����. �������� ������ �������� ���������. �������� ���������, ����������.

//20 ����, �������, 12-54

//===================================================================================================================
//===================================================================================================================

//�������� ����������. ������������� ����������.

//20 ����, �������, 13-47

//===================================================================================================================
//===================================================================================================================

//������� ��������� ����� �����. ������ ���������.

//22 ����, �����������, 23-05

//===================================================================================================================
//===================================================================================================================

//������� ������ ��� �������.

//23 ����, �������, 00-40


//===================================================================================================================
//===================================================================================================================

//��������� ���������� ���. ���� ������ �������� �� ��������� ����� �������.

//12 �������, �����

//===================================================================================================================
//===================================================================================================================

//����������� ���� ���������� ���, ����������� � ���������� �����.

//13 �������, �������, 13-40 ���

//===================================================================================================================
//===================================================================================================================

//������ � �������� � ���������� ���. �����������.

//14 �������, �������, 11-13 ���

//===================================================================================================================
//===================================================================================================================

//������� ����� � ������, �� ��, ��� IslandBlockStorage �� ��������, ������ ��� ���������� ��.
//������� ���������� ������� (��� � �����). ����� ����, �� ���� ��������� ��, ��� ���� (�� ����)

//17 �������, �������

//===================================================================================================================
//===================================================================================================================

//������������� ������ � ��������, ��������� ���� ����������
//������������� ��������� ������ ���������� �� 1.1

//18 �������, �����������

//===================================================================================================================
//===================================================================================================================

//������� ���� �� ������, ����������� ����

//���������� � ����������� ����, ����������� ����

//�������� �������� ��� � IslandBlockStorage, ����������� ����

//19 �������, �����������

//===================================================================================================================
//===================================================================================================================

//���������� ��������� �� ����

//20 �������, �������

//===================================================================================================================
//===================================================================================================================

//�������� ���� ���, ������� ����������� ����


//24 �������, �������

//===================================================================================================================
//===================================================================================================================

//������� TempVis2

//3 �������, �����������

//===================================================================================================================
//===================================================================================================================

//�������� �� TempVis2 ������ �������� ��������� ����, ����� ��������������� ��� ����. ���������, ��� ����� �������

//4 �������, �������

//===================================================================================================================
//===================================================================================================================

//����� ���� �� ��������� ���. ��������, ���������� ������, ������ ��� ������

//7-14 �������

//===================================================================================================================
//===================================================================================================================
