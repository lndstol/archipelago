
//=================================================================================================
//=== AEngine  is a part of  Archipelago  by  SSGL Gamedev Studio Copyright (C) 2012-2014  ========
//=================================================================================================
//
// This file is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// This file is managed by 
// - Leonid Stolyarov [lndstol]     lndstol@gmail.com
//
//=================================================================================================

#ifndef _BUILDOPTIONS_H_
#define _BUILDOPTIONS_H_

//=================================================================================================

// This constant is for balancing between performance and code security checks, like memory control.
// Correct value is integer in [0, 2].
//   * 0 - Disables all checks (the highest performance, not recommended).
//   * 1 - Enables basic checks (recommended, arrays bounds check).
//   * 2 - Full debug control (Very slow with huge memory overhead).

#define AENGINE_RUNTIME_CHECKS    2

//=================================================================================================

#endif
