
//=================================================================================================
//=== AEngine  is a part of  Archipelago  by  SSGL Gamedev Studio Copyright (C) 2012-2014  ========
//=================================================================================================
//
// This file is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// This file is managed by 
// - Leonid Stolyarov [lndstol]     lndstol@gmail.com
//
//=================================================================================================

#ifndef _ACQUIRABLERESOURCE_H_
#define _ACQUIRABLERESOURCE_H_

#include "AEngineDefinitions.h"

namespace AEngine {
namespace Generics {
//=================================================================================================

interface AcquirableResource
{
protected:
    AcquirableResource(const AcquirableResource&);
    AcquirableResource(const AcquirableResource&&);
};

//=================================================================================================
} }

#endif
