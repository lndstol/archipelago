
//=================================================================================================
//=== AEngine  is a part of  Archipelago  by  SSGL Gamedev Studio Copyright (C) 2012-2014  ========
//=================================================================================================
//
// This file is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// This file is managed by 
// - Leonid Stolyarov [lndstol]     lndstol@gmail.com
//
//=================================================================================================

#ifndef _AENGINEDEFINITIONS_H_
#define _AENGINEDEFINITIONS_H_

namespace AEngine {
//=================================================================================================
//=== Define platform / compiler independent types

typedef __int8   Int8;
typedef __int16  Int16;
typedef __int32  Int32;
typedef __int64  Int64;

typedef unsigned __int8   Byte;
typedef unsigned __int16  UInt16;
typedef unsigned __int32  UInt32;
typedef unsigned __int64  UInt64;

typedef UInt32  UInt;
typedef Int64   Long;
typedef Int32   Int;
typedef UInt32  Natural;
typedef float   Single;
typedef double  Real;


//=================================================================================================
//=== Define some syntax sugar

#define interface    struct

//=================================================================================================
}

#endif
