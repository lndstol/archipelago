
//=================================================================================================
//=== AEngine  is a part of  Archipelago  by  SSGL Gamedev Studio Copyright (C) 2012-2014  ========
//=================================================================================================
//
// This file is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// This file is managed by 
// - Leonid Stolyarov [lndstol]     lndstol@gmail.com
//
//=================================================================================================

#ifndef _ARRAY_H_
#define _ARRAY_H_

#include <cstdlib>
#include <cstring>
#include <typeinfo>
#include <type_traits>

#include "../BuildOptions.h"
#include "AEngineDefinitions.h"

namespace AEngine {
namespace Generics {
//=================================================================================================
// This file defines some wrappers over C++ arrays. In release mode these wrappers are completely
// transparent. In debug they provide some diagnostics like bounds-checking.
// Usage examples:
//     Array<Int, StaticAllocator<Int, 100>> a;
//     Array<Int, HeapAllocator<Int>> b(100000);

// Dummy class used for compile-time template parameter check. All memory allocators are derived
// from MemoryAllocator so Array<> can check if T_allocator is really what it's expected to be.
interface MemoryAllocator { };


template<typename T_element, Natural T_size>
class StaticAllocator : MemoryAllocator
{
    T_element data[T_size];

public:
    void Alloc(Natural sizeInElements);
    
    inline Natural Size() const { return T_size; }
    inline T_element* DataPointer() { return data; }
};


template<typename T_element>
class HeapAllocator : MemoryAllocator
{
    T_element* data;
    Natural allocatedElements;

protected:
    void copyFrom(const T_element *source);

public:
    HeapAllocator() : data(nullptr), allocatedElements(0) { };
    ~HeapAllocator();
    void Alloc(Natural sizeInElements);

    HeapAllocator(const HeapAllocator<T_element>&);
    HeapAllocator(const HeapAllocator<T_element>&&);
    
    inline Natural Size() const { return allocatedElements; }
    inline T_element* DataPointer() { return data; }
};

//=================================================================================================

template<typename T_element, typename T_allocator = HeapAllocator<T_element>>
class Array
{
    static_assert(std::is_base_of<MemoryAllocator, T_allocator>::value, 
                  "Invalid T_allocator passed: T_allocator has to be MemoryAllocator like StaticMemoryAllocator or HeapMemoryAllocator.");
    
    T_allocator allocator;

public:
    Array() { };
    Array(Natural sizeInElements);

    inline Natural Size() const { return allocator.Size(); }

#if AENGINE_RUNTIME_CHECKS == 0
    inline T_element& operator[](Natural position) { return allocator.DataPointer()[position]; };
#else
    T_element& operator[](Natural position);
#endif

protected:
    Array(const Array&);      // Disable copying arrays via copy constructor. It's more efficient to use Copy() instead.
    Array(const Array&&);
};

//=================================================================================================

template <typename T_element, Natural T_size>
class StaticArray : public Array<T_element, StaticAllocator<T_element, T_size>> { };

template <typename T_element>
class HeapArray : public Array<T_element, HeapAllocator<T_element>> 
{
public:
    HeapArray(Natural sizeInElements) : Array(sizeInElements) { }
};


//=================================================================================================
//=== Implementation goes here because of templates
//=================================================================================================


template<typename T_element, Natural T_size>
void StaticAllocator<T_element, T_size>::Alloc(Natural sizeInElements)
{
    if (sizeInElements != T_size) {
        // exception
    }
}

//=================================================================================================

template<typename T_element>
void HeapAllocator<T_element>::copyFrom(const T_element* source)
{
    if (std::is_trivially_copyable<T_element>::value) {
        std::memcpy(data, source, allocatedElements * sizeof T_element);
    }
    else {
        for (int i = 0; i < allocatedElements; ++i) data[i] = source[i];
    }
}


template<typename T_element>
HeapAllocator<T_element>::HeapAllocator(const HeapAllocator<T_element>& source)
{
    ~HeapAllocator();
    Alloc(source.allocatedElements);
    copyFrom(source.data);
}


template<typename T_element>
HeapAllocator<T_element>::HeapAllocator(const HeapAllocator<T_element>&& source)
{
    data = source.data;
    allocatedElements = source.allocatedElements;
    source.allocatedElements = 0;
}


template<typename T_element>
HeapAllocator<T_element>::~HeapAllocator()
{
    if (allocatedElements) {
        delete[] data;
        allocatedElements = 0;
    }
}


template<typename T_element>
void HeapAllocator<T_element>::Alloc(Natural sizeInElements)
{
    if (sizeInElements == allocatedElements) return;

    T_element* newData = new T_element[sizeInElements];
    if (!allocatedElements) {
        data = newData;
        allocatedElements = sizeInElements;
        return;
    }

    if (sizeInElements < allocatedElements) {
        allocatedElements = sizeInElements;
        return;
    }

    auto curData = data;
    copyFrom(curData);
    allocatedElements = sizeInElements;
}

//=================================================================================================

#if AENGINE_RUNTIME_CHECKS != 0

template<typename T_element, typename T_allocator>
T_element& Array<T_element, T_allocator>::operator[](Natural position)
{
    return allocator.DataPointer()[position];
}

#endif


template<typename T_element, typename T_allocator>
Array<T_element, T_allocator>::Array(Natural sizeInElements)
{
    allocator.Alloc(sizeInElements);
}

//=================================================================================================
} }

#endif
