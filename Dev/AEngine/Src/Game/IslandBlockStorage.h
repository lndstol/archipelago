
#pragma once

#include "stdafx.h"
#include "BlockColumn.h"

//========================================================================================
// Island block storage
//========================================================================================


class IslandBlockStorage
{
private:
	static const UInt16 initialStripCount  = 16;
	static const UInt16 initialWorldHeight = 1024;
	static const UInt16 chunkSide_         = 256;

	BlockColumn* blockColumns_;
	UInt16 groundWidthInChunks_;
	UInt16 groundHeightInChunks_;
	UInt16 groundWidth_;

	byte*    chunksState_;
	UInt16*  interpolationCurves_;

	char directoryName_[MAX_PATH];


private:
	__forceinline int FloatRound_ (float f) { f += 0.5; return _mm_cvtt_ss2si (_mm_load_ss (&f)); }
	void InitInterpolationCurve_ (UInt16 chunkX, UInt16 chunkY);
	void ReBuildInterpolationCurve_ (UInt16 chunkX, UInt16 chunkY);
	void ReCalcRenderFacesCount_ (UInt16 chunkX, UInt16 chunkY);
	void InitBlockColumns_ (UInt16 groundWidth, UInt16 groundHeight);

	void SaveChunk2Disk_ (UInt16 chunkX, UInt16 chunkY, const char* fileName);
	void LoadChunkFromFile_ (UInt16 chunkX, UInt16 chunkY, const char* fileName);
	void ReadInterpolationCurve_ (UInt16 chunkX, UInt16 chunkY, StreamBinaryBuffer* buffer);
	void WriteInterpolationCurve_ (UInt16 chunkX, UInt16 chunkY, StreamBinaryBuffer* buffer);
	const char* MakeChunkFileName_ (UInt16 chunkX, UInt16 chunkY);
	inline void EnsureChunkIsLoaded_ (UInt16 columnX, UInt16 columnY);

	inline byte HasChunkBeenChanged_ (UInt16 chunkX, UInt16 chunkY) { return chunksState_ [chunkX + chunkY * groundWidthInChunks_]; }
	inline void ChunkHasBeenChanged_ (UInt16 chunkX, UInt16 chunkY) { chunksState_ [chunkX + chunkY * groundWidthInChunks_] = 1; }

	inline BlockColumn* Column_ (UInt16 x, UInt16 y) const  { return blockColumns_ + x + y * groundWidth_; }
	inline bool ChunkIsLoaded_ (UInt16 chunkX, UInt16 chunkY) const { return Column_ (chunkX * chunkSide_ + 1, chunkY * chunkSide_ + 1)->Initialized(); }


public:
	IslandBlockStorage (UInt16 groundWidth, UInt16 groundHeight);
	IslandBlockStorage (UInt16 groundWidth, UInt16 groundHeight, const char* directoryName);
	IslandBlockStorage (IslandBlockStorage& source);
	~IslandBlockStorage();

	IslandBlockStorage* StaticCopy();

	BlockColumn* Column (UInt16 x, UInt16 y);
	void Optimize();
	void ReCalcRenderFacesCount();

	void  Set (UInt16 x, UInt16 y, UInt16 bottom, UInt16 topNext, UInt16 block);
	void  Set (UInt16 x, UInt16 y, UInt16 z, UInt16 block);
	UInt16 At  (UInt16 x, UInt16 y, UInt16 z);
	UInt16 RenderFacesCount (UInt16 x, UInt16 y);
	
	void ColumnWasChanged_ (UInt16 x, UInt16 y);
	UInt16* InterpolationCurve_ (UInt16 x, UInt16 y);

	inline UInt16 GroundWidth()  const { return groundWidth_; }
	inline UInt16 GroundHeight() const { return groundHeightInChunks_ * chunkSide_; }
	inline const char* DirectoryName() const { return directoryName_; }

	int NeedsSaving();
	void Save2Disk (const char* directoryName);
};

