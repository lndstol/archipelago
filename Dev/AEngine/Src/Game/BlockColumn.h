
#pragma once

#include "../Core/AEngineDefinitions.h"
#include "StreamBinaryBuffer.h"

//========================================================================================
// BlockColumn definition
//========================================================================================

using namespace AEngine;

class BlockColumn
{
public:
	struct BlockStrip
	{
	public:
		UInt16 Start;
		UInt16 Block;
	};

    
private:
	static const int additionalAllocSize = 4;
	BlockStrip* strips_;

	UInt16 size_;
	UInt16 allocatedSize_;
	UInt16 renderFacesCount_;


private:
	void ReAlloc_ (UInt16 newSize);
	void InsertNewStrip_ (UInt16 insertAt);
	void InsertNewStripRange_ (UInt16 insertAt, UInt16 rangeLength);
	void RemoveStrip_ (UInt16 removeAt);
	void RemoveStripRange_ (UInt16 beginAt, UInt16 endAt);
	UInt16 CalcRenderFacesCountForNeighbour_ (BlockColumn* neighbour, UInt16* interpolationCurve);


public:
	BlockColumn();
	~BlockColumn();

	void Initialize (UInt16 initialSize);
	void Initialize (StreamBinaryBuffer* buffer);
	void Initialize (BlockColumn* source);

	UInt16 StripCount() const;
	BlockStrip Strip (UInt16 index) const;
	BlockStrip* StripPtr_ (UInt16 index) const;
	int MaxHeight() const;

	inline bool Initialized() const { return strips_ != nullptr; }
	inline UInt16 RenderFacesCount() const { return renderFacesCount_;  }

	inline UInt16 StripLength (UInt16 stripIndex) const { return stripIndex >= size_ - 1 ? USHRT_MAX - MaxHeight() : strips_[stripIndex+1].Start - strips_[stripIndex].Start; }
	inline UInt16 StripEnd (UInt16 stripIndex)    const { return stripIndex >= size_ - 1 ? USHRT_MAX : strips_[stripIndex+1].Start;}

	int InStrip (UInt16 height, UInt16 lowerBound, UInt16 higerBound, UInt16* interpolationCurve) const;
	UInt16 At (UInt16 height, UInt16* interpolationCurve) const;
	void Set (UInt16 height, UInt16 blockType, UInt16* interpolationCurve);
	void Set (UInt16 bottom, UInt16 topNext, UInt16 blockType, UInt16* interpolationCurve);

	void ReCalcRenderFacesCount (BlockColumn* right, BlockColumn* left, BlockColumn* top, BlockColumn* bottom, UInt16* interpolationCurve);

	UInt32 Write2Buffer_Size() const;
	void Write2Buffer (StreamBinaryBuffer* buffer);
};

