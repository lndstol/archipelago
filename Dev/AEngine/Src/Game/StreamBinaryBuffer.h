
#pragma once

#include "stdafx.h"

//========================================================================================
// Stream binary buffer
//========================================================================================

//Set this to 1 to enable runtime bounds checking (it affects the performance)
#define StreamBinaryBuffer_RuntimeBoundsCheck  1


class StreamBinaryBuffer
{
private:
	byte* start_;
	byte* data_;
	UInt32 length_;


private:
	void CheckEnd_ (UInt32 estimatedSize);


public:
	StreamBinaryBuffer (UInt32 length);
	~StreamBinaryBuffer();

	inline UInt32 Length()    const { return length_; }
	inline byte*  Beginning() const { return start_; }
	inline bool   End()       const { return data_ == start_ + length_; }
	inline UInt32 Position()  const { return data_ - start_; }

	void WriteUInt16 (UInt16 value);
	void WriteUInt32 (UInt32 value);
	void WriteByte (byte value);
	void WriteByteArray (byte* array, UInt32 size);
	void WriteUInt32Array (UInt32* array, UInt32 size);
	void WriteUInt16Array (UInt16* array, UInt32 size);

	UInt16 ReadUInt16();
    UInt32 ReadUInt32();
	void ReadUInt16Array (UInt16* array, UInt32 size);
	void ReadUInt32Array (UInt32* array, UInt32 size);

	void MoveTo (UInt32 position);
	void Rewind();
	void Extend (UInt32 sizeDelta, bool saveData = true);
};

