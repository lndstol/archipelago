
#pragma once

#include "stdafx.h"

//========================================================================================
// Memory mapped file
//========================================================================================


class MemoryMappedFile
{
public:
	enum Access
	{
		ReadOnly  = 1,
		WriteOnly = 2,
		ReadWrite = 3
	};


private:
	HANDLE fileHandle_;
	HANDLE mappingHandle_;
	Access fileAccess_;


private: 
	MemoryMappedFile (MemoryMappedFile&);


public:
	MemoryMappedFile (const char* fileName, Access fileAccess = ReadWrite, int fileSize = 0);
	~MemoryMappedFile();

	void* MapView (int offset = 0, int length = 0);
	void UnmapView (void* pointer);

	void CloseMapping();
	void TrimFile (UInt32 neededSize);
	UInt32 GetSize();
};


