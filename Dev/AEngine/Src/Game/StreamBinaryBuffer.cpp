
#include "StreamBinaryBuffer.h"

//========================================================================================


StreamBinaryBuffer::StreamBinaryBuffer (UInt32 length) : start_ (null), data_ (null), length_ (length)
{
	start_ = data_ = (byte*) malloc (length);
	if (!start_) _throw ("Memory allocation failed.");
}


StreamBinaryBuffer::~StreamBinaryBuffer()
{
	if (start_) free (start_);
}


void StreamBinaryBuffer::CheckEnd_ (UInt32 estimatedSize)
{
	if (data_ + estimatedSize > start_ + length_) _throw ("An attempt to read/write outside the buffer.");
}


void StreamBinaryBuffer::Rewind()
{
	data_ = start_;
}


void StreamBinaryBuffer::MoveTo (UInt32 position)
{
#if StreamBinaryBuffer_RuntimeBoundsCheck
	if (position > length_) _throw ("Invalid position");
#endif

	data_ = start_ + position;
}


void StreamBinaryBuffer::Extend (UInt32 sizeDelta, bool saveData /* = true */)
{
	byte* newStart = (byte*) malloc (length_ + sizeDelta);
	if (!newStart) _throw ("Memory allocation failed.");

	if (saveData)
		for (UInt32 i = 0; i < length_; ++i) newStart[i] = start_[i];
	
	free (start_);
	length_ += sizeDelta;
	start_ = newStart;
}


void StreamBinaryBuffer::WriteUInt32 (UInt32 value)
{
#if StreamBinaryBuffer_RuntimeBoundsCheck
	CheckEnd_ (sizeof (UInt32));
#endif

	*((UInt32*) data_) = value;
	data_ += sizeof (UInt32);
}


void StreamBinaryBuffer::WriteUInt16 (UInt16 value)
{
#if StreamBinaryBuffer_RuntimeBoundsCheck
	CheckEnd_ (sizeof (UInt16));
#endif

	*((UInt16*) data_) = value;
	data_ += sizeof (UInt16);
}


void StreamBinaryBuffer::WriteByte (byte value)
{
#if StreamBinaryBuffer_RuntimeBoundsCheck
	CheckEnd_ (sizeof (byte));
#endif

	*(data_++) = value;
}


void StreamBinaryBuffer::WriteUInt16Array (UInt16* array, UInt32 length)
{
#if StreamBinaryBuffer_RuntimeBoundsCheck
	CheckEnd_ (sizeof (UInt16) * length);
#endif

	UInt16* data = (UInt16*) data_;
	for (UInt32 i = 0; i < length; ++i) data[i] = array[i];
	data_ += sizeof (UInt16) * length;
}


void StreamBinaryBuffer::WriteUInt32Array (UInt32* array, UInt32 length)
{
#if StreamBinaryBuffer_RuntimeBoundsCheck
	CheckEnd_ (sizeof (UInt32) * length);
#endif

	UInt32* data = (UInt32*) data_;
	for (UInt32 i = 0; i < length; ++i) data[i] = array[i];
	data_ += sizeof (UInt32) * length;
}


UInt16 StreamBinaryBuffer::ReadUInt16()
{
#if StreamBinaryBuffer_RuntimeBoundsCheck
	CheckEnd_ (sizeof (UInt16));
#endif

	data_ += sizeof (UInt16);
	return *((UInt16*) (data_ - sizeof (UInt16)));
}


UInt32 StreamBinaryBuffer::ReadUInt32()
{
#if StreamBinaryBuffer_RuntimeBoundsCheck
	CheckEnd_ (sizeof (UInt32));
#endif

	data_ += sizeof (UInt32);
	return *((UInt32*) (data_ - sizeof (UInt32)));
}


void StreamBinaryBuffer::ReadUInt16Array (UInt16* array, UInt32 size)
{
#if StreamBinaryBuffer_RuntimeBoundsCheck
	CheckEnd_ (sizeof (UInt16) * size);
#endif

	UInt16* data = (UInt16*) data_;
	for (UInt32 i = 0; i < size; ++i) array[i] = data[i];
	data_ += sizeof (UInt16) * size;
}


void StreamBinaryBuffer::ReadUInt32Array (UInt32* array, UInt32 size)
{
#if StreamBinaryBuffer_RuntimeBoundsCheck
	CheckEnd_ (sizeof (UInt32) * size);
#endif

	UInt32* data = (UInt32*) data_;
	for (UInt32 i = 0; i < size; ++i) array[i] = data[i];
	data_ += sizeof (UInt32) * size;
}


