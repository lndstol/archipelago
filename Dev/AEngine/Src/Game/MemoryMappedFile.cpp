
#include "MemoryMappedFile.h"

//========================================================================================


MemoryMappedFile::MemoryMappedFile (const char* fileName, MemoryMappedFile::Access fileAccess, int fileSize) : 
	fileHandle_ (NULL), mappingHandle_ (NULL), fileAccess_ (fileAccess)
{
	DWORD accessDword = NULL;
	if (fileAccess == ReadOnly)  accessDword = GENERIC_READ;
	if (fileAccess == WriteOnly) accessDword = GENERIC_WRITE;
	if (fileAccess == ReadWrite) accessDword = GENERIC_READ | GENERIC_WRITE;

	fileHandle_ = CreateFile (fileName, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_DELETE, NULL, fileAccess == WriteOnly ? CREATE_ALWAYS : OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	DWORD err = GetLastError();
	if (!fileHandle_) _throw ((std::string ("Can't open file '") + fileName + "'").c_str());

	DWORD mappingProtect = NULL;
	if (fileAccess == ReadOnly)  mappingProtect = PAGE_READONLY;
	if (fileAccess == WriteOnly) mappingProtect = PAGE_WRITECOPY;
	if (fileAccess == ReadWrite) mappingProtect = PAGE_READWRITE;

	//if (fileSize == 0) fileSize = GetSize();
	mappingHandle_ = CreateFileMapping (fileHandle_, NULL, PAGE_READWRITE, 0, fileSize, NULL);
	err = GetLastError();
	if (!mappingHandle_) _throw ((std::string ("Can't create file mapping for '") + fileName + "'").c_str());
}


void* MemoryMappedFile::MapView (int offset, int length)
{
	DWORD accessDword = NULL;
	if (fileAccess_ == ReadOnly)  accessDword = FILE_MAP_READ;
	if (fileAccess_ == WriteOnly) accessDword = FILE_MAP_WRITE;
	if (fileAccess_ == ReadWrite) accessDword = FILE_MAP_ALL_ACCESS;

	void* dataPointer = (void*) MapViewOfFile (mappingHandle_, accessDword, 0, offset, length);
	if (!dataPointer) _throw ("File view mapping failed.");

	return dataPointer;
}


void MemoryMappedFile::UnmapView (void* pointer)
{
	UnmapViewOfFile (pointer);
}


void MemoryMappedFile::CloseMapping()
{
	CloseHandle (mappingHandle_);
	mappingHandle_ = NULL;
}


void MemoryMappedFile::TrimFile (UInt32 neededSize)
{
	if (mappingHandle_) CloseMapping();
	
	SetFilePointer (fileHandle_, neededSize, 0, FILE_BEGIN);
	if (!SetEndOfFile (fileHandle_)) _throw ("File size modification failed.");
}


UInt32 MemoryMappedFile::GetSize()
{
	return GetFileSize (fileHandle_, 0);
}


MemoryMappedFile::~MemoryMappedFile()
{
	if (mappingHandle_) CloseHandle (mappingHandle_);
	CloseHandle (fileHandle_);
}

