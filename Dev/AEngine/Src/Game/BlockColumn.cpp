
#include "BlockColumn.h"
#include "IslandBlockStorage.h"

//========================================================================================


BlockColumn::BlockColumn() : strips_ (null), size_ (1), allocatedSize_ (0), renderFacesCount_ (0)
{ }


BlockColumn::~BlockColumn()
{
	if (strips_) free (strips_);
}


void BlockColumn::Initialize (UInt16 initialSize)
{
	ReAlloc_ (initialSize + 1);
	*((unsigned __int32*) strips_) = Block_Air;
}


void BlockColumn::Initialize (BlockColumn* source)
{
	ReAlloc_ (source->size_ + 1);
	UInt32* data_ = (UInt32*) strips_;
	UInt32* sourceData_ = (UInt32*) source->strips_;

	for (UInt16 i = 0; i < source->size_; ++i) data_[i] = sourceData_[i];
}


UInt16 BlockColumn::StripCount() const
{
	return size_;
}


BlockColumn::BlockStrip BlockColumn::Strip (UInt16 index) const
{
	// No bounds checking here because of performance reasons
	return strips_[index];
}


BlockColumn::BlockStrip* BlockColumn::StripPtr_ (UInt16 index) const
{
	return strips_ + index;
}


int BlockColumn::MaxHeight() const
{
	return strips_[size_-1].Start;
}


//========================================================================================


void BlockColumn::ReAlloc_ (UInt16 newSize)
{
	BlockStrip* oldRanges = strips_;
	if (allocatedSize_ >= USHRT_MAX-2*additionalAllocSize) _throw ("Column overflow. Strip count should be less then UInt16.MaxValue");

	strips_ = (BlockStrip*) malloc (sizeof (BlockStrip) * newSize);
	if (!strips_) _throw ("Memory allocation failed.");
 
	if (allocatedSize_ != 0)
	{
		for (UInt16 i = 0; i < size_; ++i) strips_[i] = oldRanges[i];
		free (oldRanges);
	}

	allocatedSize_ = newSize;
}


void BlockColumn::InsertNewStrip_ (UInt16 insertAt)
{
	if (size_ >= allocatedSize_) ReAlloc_ (allocatedSize_ + additionalAllocSize);
	for (UInt16 i = size_++; i > insertAt; --i) strips_[i] = strips_[i-1];
}


void BlockColumn::InsertNewStripRange_ (UInt16 insertAt, UInt16 rangeLength)
{
	if (size_ + rangeLength-1 >= allocatedSize_) ReAlloc_ (allocatedSize_ + rangeLength-1 + additionalAllocSize);
	
	for (UInt16 i = size_-1+rangeLength; i > insertAt + rangeLength; --i) strips_[i] = strips_[i - rangeLength];
	size_ += (UInt16) rangeLength;
}


void BlockColumn::RemoveStrip_ (UInt16 removeAt)
{
	for (UInt16 i = removeAt+1; i < size_; ++i) strips_[i-1] = strips_[i];
	--size_;
}


void BlockColumn::RemoveStripRange_ (UInt16 beginAt, UInt16 endAt)
{
	int removeSize = endAt - beginAt;
	if (removeSize <= 0) return;

	for (int i = endAt; i < size_; ++i) strips_[i - removeSize] = strips_[i];
	size_ -= (UInt16) removeSize;
}


//========================================================================================


int BlockColumn::InStrip (UInt16 height, UInt16 lowerBound, UInt16 higerBound, UInt16* interpolationCurve) const
{
	UInt16 size_m1 = size_-1;
	if (height >= strips_[size_m1].Start) return size_m1;

	int middle = interpolationCurve[height];
	if (middle >= size_) middle = size_m1;

	do
	{
		int middleStart = strips_[middle].Start;
		if (middleStart <= height)     if (strips_[middle+1].Start > height)  return middle;   else lowerBound = middle+1;
		else                           if (height >= strips_[middle-1].Start) return middle-1; else higerBound = middle-1;

		middle = (lowerBound + higerBound) / 2;
	}
	while (strips_[lowerBound].Start <= height && strips_[higerBound].Start > height);

	return middle;
}


UInt16 BlockColumn::At (UInt16 height, UInt16* interpolationCurve) const
{
	return strips_[InStrip (height, 0, size_-1, interpolationCurve)].Block;
}


void BlockColumn::Set (UInt16 height, UInt16 blockType, UInt16* interpolationCurve)
{
	UInt16 stripIndex = height > strips_[size_-1].Start ? size_-1 : InStrip (height, 0, size_-1, interpolationCurve);
	if (strips_[stripIndex].Block == blockType) return;

	if (height == strips_[stripIndex].Start)
	{
		if (stripIndex > 0 && blockType == strips_[stripIndex-1].Block) { strips_[stripIndex].Start += 1; return; }

		InsertNewStrip_ (stripIndex);
		strips_[stripIndex].Start = height;
		strips_[stripIndex].Block = blockType;
		strips_[stripIndex+1].Start += 1;

		return;
	}

	if (stripIndex < size_ -1 && height == strips_[stripIndex+1].Start-1)
	{
		if (blockType == strips_[stripIndex+1].Block) { strips_[stripIndex+1].Start -= 1; return; }

		InsertNewStrip_ (stripIndex+1);
		strips_[stripIndex+1].Block  = blockType;
		strips_[stripIndex+1].Start = height;

		return;
	}

	InsertNewStripRange_ (stripIndex, 2);
	strips_[stripIndex+2].Block  = strips_[stripIndex].Block;
	strips_[stripIndex+2].Start = (height + 1);
	strips_[stripIndex+1].Start = height;
	strips_[stripIndex+1].Block  = blockType;
}


void BlockColumn::Set (UInt16 bottom, UInt16 topNext, UInt16 blockType, UInt16* interpolationCurve)
{
	UInt16 lowerStripIndex, higerStripIndex;
	if (bottom < strips_[size_-1].Start)
	{
		lowerStripIndex = InStrip (bottom, 0, size_-1, interpolationCurve);
		higerStripIndex = lowerStripIndex < size_-1 && topNext < strips_[lowerStripIndex+1].Start ? lowerStripIndex : InStrip (topNext, lowerStripIndex, size_-1, interpolationCurve);
	}
	else lowerStripIndex = higerStripIndex = size_-1;

	char fitTop      = higerStripIndex < size_-1 && topNext == strips_[higerStripIndex+1].Start-1;
	char fitBottom   = bottom == strips_[lowerStripIndex].Start;

	char mergeBottom = fitBottom && lowerStripIndex > 0 && blockType == strips_[lowerStripIndex-1].Block;
	char mergeTop    = fitTop && blockType == strips_[higerStripIndex].Block;

	if (lowerStripIndex == higerStripIndex)
	{
		if (fitTop && fitBottom)
		{ 
			if (mergeTop && mergeBottom) { RemoveStripRange_ (lowerStripIndex, lowerStripIndex+2); return; }
			if (mergeTop)                { RemoveStrip_ (lowerStripIndex+1); return; }
			if (mergeBottom)             { RemoveStrip_ (lowerStripIndex);   return; } 

			strips_[lowerStripIndex].Block = blockType; return; 
		}

		if (fitBottom)
		{ 
			if (mergeBottom) { strips_[lowerStripIndex].Start = topNext; return; }

			strips_[lowerStripIndex].Start = topNext; 
			InsertNewStrip_ (lowerStripIndex); 
			strips_[lowerStripIndex].Start = bottom;
			strips_[lowerStripIndex].Block = blockType;
			return;
		}

		if (fitTop)
		{
			if (mergeTop) { strips_[lowerStripIndex].Start = bottom; return; }

			InsertNewStrip_ (lowerStripIndex+1);
			strips_[lowerStripIndex+1].Start = bottom;
			strips_[lowerStripIndex+1].Block  = blockType;
			return;
		}

		if (*((UInt16*) &strips_[lowerStripIndex].Block) == blockType) return;

		InsertNewStripRange_ (lowerStripIndex, 2);
		strips_[lowerStripIndex+1].Start = bottom;
		strips_[lowerStripIndex+1].Block  = blockType;
		strips_[lowerStripIndex+2].Start = topNext;
		strips_[lowerStripIndex+2].Block  = strips_[lowerStripIndex].Block;
		return;
	}

	else
	{
		if (fitTop && fitBottom)
		{ 
			if (mergeTop && mergeBottom) { RemoveStripRange_ (lowerStripIndex, higerStripIndex+2); return; }
			if (mergeTop)                { RemoveStripRange_ (lowerStripIndex, higerStripIndex+1); strips_[lowerStripIndex].Start = bottom; return; }
			if (mergeBottom)             { RemoveStripRange_ (lowerStripIndex, higerStripIndex+1); return; }

			RemoveStripRange_ (lowerStripIndex + 1, higerStripIndex + 1); 
			strips_[lowerStripIndex].Block = blockType;
			return; 
		}

		RemoveStripRange_ (lowerStripIndex+1, higerStripIndex - (!fitBottom && !fitTop ? 1 : 0));

		if (fitBottom)
		{
			if (mergeBottom) { RemoveStrip_ (lowerStripIndex); strips_[lowerStripIndex].Start = topNext; return; }

			strips_[lowerStripIndex].Block = blockType;
			strips_[lowerStripIndex+1].Start = topNext;
			return;
		}

		if (fitTop)
		{
			if (mergeTop) { RemoveStrip_ (lowerStripIndex+1); strips_[lowerStripIndex+1].Start = bottom; return; }

			strips_[lowerStripIndex+1].Block = blockType;
			strips_[lowerStripIndex+1].Start = bottom;
			return;
		}

		if (higerStripIndex - lowerStripIndex == 1) InsertNewStrip_ (higerStripIndex);
		strips_[lowerStripIndex+1].Start = bottom;
		strips_[lowerStripIndex+1].Block = blockType;
		strips_[lowerStripIndex+2].Start = topNext;
		return;
	}
}


UInt16 BlockColumn::CalcRenderFacesCountForNeighbour_ (BlockColumn* neighbour, UInt16* interpolationCurve)
{
	UInt16 result = 0;

	for (UInt16 i = 0; i < neighbour->StripCount(); ++i)
	{
		if (neighbour->Strip (i).Block != Block_Air) continue;
		UInt16 stripIdBegin = InStrip (neighbour->Strip (i).Start, 0, StripCount(), interpolationCurve);
		
		if (StripEnd (stripIdBegin) >= neighbour->StripEnd (i))
		{
			if (strips_[stripIdBegin].Block == Block_Air) continue;
			result += neighbour->StripLength (i);
			continue;
		}

		else
		{
			UInt16 stripIdEnd = InStrip (neighbour->StripEnd (i), stripIdBegin, StripCount(), interpolationCurve);
			result += neighbour->StripLength (i);
			
			if (strips_[stripIdBegin].Block == Block_Air) result -= StripEnd (stripIdBegin) - neighbour->Strip (i).Start;
			if (strips_[stripIdEnd].Block   == Block_Air) result -= neighbour->StripEnd (i) - strips_[stripIdEnd].Start;

			for (UInt16 j = stripIdBegin+1; j < stripIdEnd; ++j) { if (strips_[j].Block == Block_Air) result -= StripLength (j); }
		}
	}

	return result;
}


void BlockColumn::ReCalcRenderFacesCount (BlockColumn* right, BlockColumn* left, BlockColumn* top, BlockColumn* bottom, UInt16* interpolationCurve)
{
	renderFacesCount_ = 0;
	for (UInt16 i = 0; i < size_; ++i) { if (strips_[i].Block == Block_Air) renderFacesCount_ += 2; }
	renderFacesCount_ -= 1;

	if (right)  renderFacesCount_ += CalcRenderFacesCountForNeighbour_ (right,  interpolationCurve);
	if (left)   renderFacesCount_ += CalcRenderFacesCountForNeighbour_ (left,   interpolationCurve);
	if (top)    renderFacesCount_ += CalcRenderFacesCountForNeighbour_ (top,    interpolationCurve);
	if (bottom) renderFacesCount_ += CalcRenderFacesCountForNeighbour_ (bottom, interpolationCurve);
}


//========================================================================================


UInt32 BlockColumn::Write2Buffer_Size() const
{
	return 2 * sizeof (UInt16) + sizeof (BlockStrip) * size_;
}


void BlockColumn::Write2Buffer (StreamBinaryBuffer* buffer)
{
	if (sizeof (BlockStrip) != sizeof (UInt32)) _throw ("Something really bad happened ... but sizeof (BlockStrip) != sizeof (UInt32)");

	buffer->WriteUInt16 (renderFacesCount_);
	buffer->WriteUInt16 (size_);
	buffer->WriteUInt32Array ((UInt32*) strips_, size_);
}


void BlockColumn::Initialize (StreamBinaryBuffer* buffer)
{
	renderFacesCount_ = buffer->ReadUInt16();
	size_ = buffer->ReadUInt16();
	
	ReAlloc_ (size_ + additionalAllocSize);
	buffer->ReadUInt32Array ((UInt32*) strips_, size_);
}



