
#pragma once

#include "stdafx.h"
#include "IslandBlockStorage.h"

//========================================================================================
// Island block storage
//========================================================================================


class IslandQuadtreeBuilder
{
public:
	#pragma pack(push, 1)
	class Node
	{
	public:
		UInt16 TopLeftChild;
		UInt16 TopRightChild;
		UInt16 BottomLeftChild;
		UInt16 BottomRightChild;
		
		UInt16 X1;
		UInt16 X2;
		UInt16 Y1;
		UInt16 Y2;
	};
	#pragma pack(pop)


protected:
	IslandBlockStorage* islandBlocks_;
	UInt16 nodeCount_;
	Node*  nodes_;
	int    maxFacesInNode_;


protected:
	void Subdivide_ (UInt16 parentNode);
	UInt16 NewNode_();
	void MakeLeaf_ (UInt16 node);
	void BoundNode_ (UInt16 node, UInt16 x1, UInt16 y1, UInt16 x2, UInt16 y2);


public:
	IslandQuadtreeBuilder (IslandBlockStorage* islandBlocks);
	~IslandQuadtreeBuilder();

	int RenderFacesCount (UInt16 x1, UInt16 y1, UInt16 x2, UInt16 y2, int maxCount) const;
	UInt16 Build (Node* nodes, int maxFacesInNode);
};

