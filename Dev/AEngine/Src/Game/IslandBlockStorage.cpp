
#include "StdAfx.h"
#include "IslandBlockStorage.h"
#include "MemoryMappedFile.h"
#include "External/quicklz.h"

//========================================================================================


IslandBlockStorage::IslandBlockStorage (UInt16 groundWidth, UInt16 groundHeight) : chunksState_ (NULL), interpolationCurves_ (NULL)
{
	strcpy_s (directoryName_, "<dynamic>");
	InitBlockColumns_ (groundWidth, groundHeight);

	for (UInt16 i = 0; i < groundWidthInChunks_; ++i)
		for (UInt16 j = 0; j < groundHeightInChunks_; ++j)
			InitInterpolationCurve_ (i, j);

	for (int i = 0; i < groundWidth; ++i)
		for (int j = 0; j < groundHeight; ++j)
			Column_ (i, j)->Initialize (initialStripCount);

}


IslandBlockStorage::IslandBlockStorage (IslandBlockStorage& source)
{
	strcpy_s (directoryName_, source.directoryName_);
	InitBlockColumns_ (source.GroundWidth(), source.GroundHeight());

	for (UInt16 i = 0; i < groundWidthInChunks_ * groundHeightInChunks_; ++i)
	{
		UInt32* interpolationCurve       = (UInt32*)(interpolationCurves_        + USHRT_MAX * i);
		UInt32* sourceInterpolationCurve = (UInt32*)(source.interpolationCurves_ + USHRT_MAX * i);
		for (UInt16 j = 0; j < USHRT_MAX/2; ++j) interpolationCurve[i] = sourceInterpolationCurve[i];
	}

	for (int i = 0; i < groundWidth_; ++i)
		for (int j = 0; j < GroundHeight(); ++j)
			Column_ (i, j)->Initialize (source.Column (i, j));

	for (int i = 0; i < groundWidthInChunks_ * groundHeightInChunks_; ++i) chunksState_[i] = source.chunksState_[i];
}


void IslandBlockStorage::InitBlockColumns_ (UInt16 groundWidth, UInt16 groundHeight)
{
	groundWidth_  = groundWidth;
	blockColumns_ = new BlockColumn[groundWidth * groundHeight];

	if (groundWidth % chunkSide_ != 0 || groundHeight % chunkSide_ != 0) _throw ("Invalid island size. Chunk division failed.");
	groundWidthInChunks_  = groundWidth  / chunkSide_;
	groundHeightInChunks_ = groundHeight / chunkSide_;

	chunksState_ = new byte[groundHeightInChunks_ * groundWidthInChunks_];
	if (!chunksState_) _throw ("Memory allocation failed.");
	for (UInt16 i = 0; i < groundHeightInChunks_ * groundWidthInChunks_; ++i) chunksState_[i] = 0;

	interpolationCurves_ = new UInt16[USHRT_MAX * groundHeightInChunks_ * groundWidthInChunks_];
	if (!interpolationCurves_) _throw ("Memory allocation failed.");
}


IslandBlockStorage::~IslandBlockStorage()
{
	if (blockColumns_)        delete[] blockColumns_;
	if (chunksState_)         delete[] chunksState_;
	if (interpolationCurves_) delete[] interpolationCurves_;
}


int IslandBlockStorage::NeedsSaving()
{
	int needsSaving = 0;

	for (UInt16 i = 0; i < groundWidthInChunks_; ++i)
		for (UInt16 j = 0; j < groundHeightInChunks_; ++j)
			if (HasChunkBeenChanged_ (i, j)) ++needsSaving;

	return needsSaving;
}


void IslandBlockStorage::Optimize()
{
	for (UInt16 i = 0; i < groundWidthInChunks_; ++i)
		for (UInt16 j = 0; j < groundHeightInChunks_; ++j)
			if (HasChunkBeenChanged_ (i, j))
			{
				ReBuildInterpolationCurve_ (i, j);
				ReCalcRenderFacesCount_ (i, j);
			}
}


void IslandBlockStorage::ReCalcRenderFacesCount()
{
	for (UInt16 i = 0; i < groundWidthInChunks_; ++i)
		for (UInt16 j = 0; j < groundHeightInChunks_; ++j)
			if (HasChunkBeenChanged_ (i, j))
				ReCalcRenderFacesCount_ (i, j);
}


void IslandBlockStorage::InitInterpolationCurve_ (UInt16 chunkX, UInt16 chunkY)
{
	UInt16* interpolationCurve = interpolationCurves_ + USHRT_MAX * (chunkX + chunkY * groundWidthInChunks_);
	UInt16 avgStripLen = initialWorldHeight / initialStripCount;

	for (UInt16 i = 0; i < initialStripCount; ++i)
		for (UInt16 j = 0; j < avgStripLen; ++j)
			interpolationCurve[i*avgStripLen + j] = i;
}


void IslandBlockStorage::ReBuildInterpolationCurve_ (UInt16 chunkX, UInt16 chunkY)
{
	UInt16* interpolationCurve = interpolationCurves_ + USHRT_MAX * (chunkX + chunkY * groundWidthInChunks_);
	int interpolCurveHeight = USHRT_MAX;
	for (UInt16 i = 0; i < interpolCurveHeight; ++i) interpolationCurve[i] = 0;

	UInt16 groundWidth  = groundWidthInChunks_  * chunkSide_;
	UInt16 groundHeight = groundHeightInChunks_ * chunkSide_;

	float probeCount = 0;
	UInt16 widthBound  = (chunkX + 1) * chunkSide_;
	UInt16 heightBound = (chunkY + 1) * chunkSide_;

	for (UInt16 i = chunkX * chunkSide_; i < widthBound; ++i)
		for (UInt16 j = chunkY * chunkSide_; j < heightBound; ++j)
		{
			BlockColumn* blockColumn = Column (i, j);
			if (blockColumn->MaxHeight() <= 1 || (i != j && groundWidth - i != groundHeight - j && i != j+1 && i+1 != j && i+2 != j && j+2 != i)) continue;

			int height = 0, strip = 0;
			++probeCount;

			while (height < interpolCurveHeight)
			{
				interpolationCurve[height++] += strip;
				if (strip < blockColumn->StripCount()-1 && height >= blockColumn->Strip (strip+1).Start) ++strip;
			}
		}

		for (int i = 0; i < interpolCurveHeight; ++i)
			interpolationCurve[i] = FloatRound_ ((float) interpolationCurve[i] / probeCount);
}


void IslandBlockStorage::ReCalcRenderFacesCount_ (UInt16 chunkX, UInt16 chunkY)
{
	EnsureChunkIsLoaded_ (chunkX * chunkSide_ + 1, chunkY * chunkSide_ + 1);
	if (chunkX < groundWidthInChunks_-1)  EnsureChunkIsLoaded_ ((chunkX+1) * chunkSide_ + 1, chunkY * chunkSide_ + 1);
	if (chunkX > 0)                       EnsureChunkIsLoaded_ ((chunkX-1) * chunkSide_ + 1, chunkY * chunkSide_ + 1);
	if (chunkY < groundHeightInChunks_-1) EnsureChunkIsLoaded_ (chunkX * chunkSide_ + 1, (chunkY+1) * chunkSide_ + 1);
	if (chunkY > 0)                       EnsureChunkIsLoaded_ (chunkX * chunkSide_ + 1, (chunkY-1) * chunkSide_ + 1);

	UInt16 xOffset = chunkX * chunkSide_;
	UInt16 yOffset = chunkY * chunkSide_;

	UInt16 groundWidth  = groundWidthInChunks_  * chunkSide_;
	UInt16 groundHeight = groundHeightInChunks_ * chunkSide_;

	for (UInt16 i = 0; i < chunkSide_; ++i)
		for (UInt16 j = 0; j < chunkSide_; ++j)
		{
			UInt16 baseX = i + xOffset;
			UInt16 baseY = j + yOffset;

			Column_ (baseX, baseY)->ReCalcRenderFacesCount (baseX < groundWidth-1  ? Column_ (baseX+1, baseY) : NULL, baseX > 0 ? Column_ (baseX-1, baseY) : NULL, 
			                                                baseY < groundHeight-1 ? Column_ (baseX, baseY+1) : NULL, baseY > 0 ? Column_ (baseX, baseY-1) : NULL,
													        InterpolationCurve_ (baseX, baseY));
		}
}


void IslandBlockStorage::ColumnWasChanged_ (UInt16 x, UInt16 y)
{
	ChunkHasBeenChanged_ (x / chunkSide_, y / chunkSide_);
}


UInt16* IslandBlockStorage::InterpolationCurve_ (UInt16 x, UInt16 y)
{
	return interpolationCurves_ + ((x / chunkSide_) + (y / chunkSide_) * groundWidthInChunks_) * USHRT_MAX;
}


BlockColumn* IslandBlockStorage::Column (UInt16 x, UInt16 y)
{
	EnsureChunkIsLoaded_ (x, y);
	return Column_ (x, y);
}


void IslandBlockStorage::Set (UInt16 x, UInt16 y, UInt16 bottom, UInt16 topNext, UInt16 block)
{
	EnsureChunkIsLoaded_ (x, y);
	Column_ (x, y)->Set (bottom, topNext, block, InterpolationCurve_ (x, y));
	ColumnWasChanged_ (x, y);
}


void IslandBlockStorage::Set (UInt16 x, UInt16 y, UInt16 z, UInt16 block)
{
	EnsureChunkIsLoaded_ (x, y);
	Column_ (x, y)->Set (z, block, InterpolationCurve_ (x, y));
	ColumnWasChanged_ (x, y);
}


UInt16 IslandBlockStorage::At (UInt16 x, UInt16 y, UInt16 z)
{
	EnsureChunkIsLoaded_ (x, y);
	return Column_ (x, y)->At (z, InterpolationCurve_ (x, y));
}


UInt16 IslandBlockStorage::RenderFacesCount (UInt16 x, UInt16 y)
{
	return Column_ (x, y)->RenderFacesCount();
}


void IslandBlockStorage::EnsureChunkIsLoaded_ (UInt16 columnX, UInt16 columnY)
{
	if (Column_ (columnX, columnY)->Initialized()) return;

	char chunkFileName[MAX_PATH] = "";
	
	sprintf_s (chunkFileName, "%s\\%s", directoryName_, MakeChunkFileName_ (columnX / chunkSide_, columnY / chunkSide_));
	LoadChunkFromFile_ (columnX / chunkSide_, columnY / chunkSide_, chunkFileName);
}


//========================================================================================


void IslandBlockStorage::ReadInterpolationCurve_ (UInt16 chunkX, UInt16 chunkY, StreamBinaryBuffer* buffer)
{
	UInt16 stripsCount = buffer->ReadUInt16();
	UInt16* interpolationCurve = InterpolationCurve_ (chunkX * chunkSide_ + 1, chunkY * chunkSide_ + 1);

	UInt16 height = 0;
	for (UInt16 i = 0; i < stripsCount; ++i)
	{
		UInt16 nextStripLen = buffer->ReadUInt16();
		for (int j = 0; j < nextStripLen; ++j) interpolationCurve[height + j] = i;
		height += nextStripLen;
	}
}


void IslandBlockStorage::WriteInterpolationCurve_ (UInt16 chunkX, UInt16 chunkY, StreamBinaryBuffer* buffer)
{
	UInt16* interpolationCurve = InterpolationCurve_ (chunkX * chunkSide_ + 1, chunkY * chunkSide_ + 1);
	buffer->WriteUInt16 (interpolationCurve[USHRT_MAX-1] + 1);

	UInt16 strip = 0;
	for (UInt16 i = 0; i < USHRT_MAX; )
	{
		int nextLen = 0;
		for (; i < USHRT_MAX && interpolationCurve[i] == strip; ++i) ++nextLen;
		buffer->WriteUInt16 (nextLen);
		++strip;
	}
}


const char* IslandBlockStorage::MakeChunkFileName_ (UInt16 chunkX, UInt16 chunkY)
{
	static char temp[MAX_PATH] = "";
	sprintf_s (temp, "p%d", chunkX + chunkY * groundWidthInChunks_);
	return temp;
}


IslandBlockStorage::IslandBlockStorage (UInt16 groundWidth, UInt16 groundHeight, const char* directoryName) : groundWidth_ (groundWidth)
{
	printf("1\n");
	strcpy_s (directoryName_, directoryName);
	InitBlockColumns_ (groundWidth, groundHeight);
}


void IslandBlockStorage::LoadChunkFromFile_ (UInt16 chunkX, UInt16 chunkY, const char* fileName)
{
	UInt16 columnOffsetX = chunkX * chunkSide_;
	UInt16 columnOffsetY = chunkY * chunkSide_;

	MemoryMappedFile chunkFile (fileName, MemoryMappedFile::ReadOnly);
	byte* fileData = (byte*) chunkFile.MapView();
	StreamBinaryBuffer buffer (qlz_size_decompressed ((char*) fileData));

	qlz_state_decompress* qlzDecompressionState = (qlz_state_decompress*) malloc (sizeof (qlz_state_decompress));
	size_t compressedSize = qlz_decompress ((char*) fileData, buffer.Beginning(), qlzDecompressionState);
	free (qlzDecompressionState);
	chunkFile.UnmapView (fileData);

	ReadInterpolationCurve_ (chunkX, chunkY, &buffer);
	for (int k = 0; k < chunkSide_; ++k) for (int l = 0; l < chunkSide_; ++l)
		Column_ (columnOffsetX + k, columnOffsetY + l)->Initialize (&buffer);
}


void IslandBlockStorage::SaveChunk2Disk_ (UInt16 chunkX, UInt16 chunkY, const char* fileName)
{
	UInt16 columnOffsetX = chunkX * chunkSide_;
	UInt16 columnOffsetY = chunkY * chunkSide_;

	UInt32 neededBufferSize = 0;
	for (int k = 0; k < chunkSide_; ++k) for (int l = 0; l < chunkSide_; ++l) 
		neededBufferSize += Column_ (columnOffsetX + k, columnOffsetY + l)->Write2Buffer_Size();
	neededBufferSize += USHRT_MAX * sizeof (UInt16); // for interpolation curve

	StreamBinaryBuffer buffer (neededBufferSize);

	WriteInterpolationCurve_ (chunkX, chunkY, &buffer);
	for (int k = 0; k < chunkSide_; ++k) for (int l = 0; l < chunkSide_; ++l)
		Column_ (columnOffsetX + k, columnOffsetY + l)->Write2Buffer (&buffer);

	MemoryMappedFile chunkFile (fileName, MemoryMappedFile::WriteOnly, buffer.Position());
	byte* fileData = (byte*) chunkFile.MapView();

	qlz_state_compress* qlzCompressionState = (qlz_state_compress*) malloc (sizeof (qlz_state_compress));
	size_t compressedSize = qlz_compress (buffer.Beginning(), (char*) fileData, buffer.Position(), qlzCompressionState);
	free (qlzCompressionState);
	
	chunkFile.UnmapView (fileData);
	chunkFile.TrimFile (compressedSize);
}


void IslandBlockStorage::Save2Disk (const char* directoryName)
{
	strcpy_s (directoryName_, directoryName_);

	for (UInt16 i = 0; i < groundWidthInChunks_; ++i)
		for (UInt16 j = 0; j < groundHeightInChunks_; ++j)
			if (HasChunkBeenChanged_ (i, j))
			{
				char chunkFileName[MAX_PATH] = "";
				sprintf_s (chunkFileName, "%s\\%s", directoryName, MakeChunkFileName_ (i, j));

				SaveChunk2Disk_ (i, j, chunkFileName);
			}
}

