
#include "IslandQuadtreeBuilder.h"

//=================================================================================


IslandQuadtreeBuilder::IslandQuadtreeBuilder (IslandBlockStorage* islandBlocks) : islandBlocks_ (islandBlocks), nodeCount_ (0), nodes_ (null)
{
	if (!islandBlocks || islandBlocks->RenderFacesCount (0, 0) == 0) 
		_throw ("Invalid island block storage. Maybe you haven't calculated render faces.");
}


IslandQuadtreeBuilder::~IslandQuadtreeBuilder()
{
}


int IslandQuadtreeBuilder::RenderFacesCount (UInt16 x1, UInt16 y1, UInt16 x2, UInt16 y2, int maxCount) const
{
	int result = 0;

	for (UInt16 i = x1; i < x2; ++i)
	{
		for (UInt16 j = y1; j < y2; ++j)
			result += islandBlocks_->RenderFacesCount (i, j);

		if (result >= maxCount) return result;
	}

	return result;
}


UInt16 IslandQuadtreeBuilder::Build (Node* nodes, int maxFacesInNode)
{
	maxFacesInNode_ = maxFacesInNode;
	nodes_          = nodes;
	nodeCount_      = 0;

	NewNode_();
	nodes_[0].X1 = nodes_[0].Y1 = 0;
	nodes_[0].X2 = islandBlocks_->GroundWidth();
	nodes_[0].Y2 = islandBlocks_->GroundHeight();

	Subdivide_ (0);
	return nodeCount_;
}


void IslandQuadtreeBuilder::Subdivide_ (UInt16 parentNode)
{
	UInt16 x1 = nodes_[parentNode].X1;
	UInt16 x2 = nodes_[parentNode].X2;
	UInt16 y1 = nodes_[parentNode].Y1;
	UInt16 y2 = nodes_[parentNode].Y2;

	UInt16 middleX = (x1 + x2) / 2;
	UInt16 middleY = (y1 + y2) / 2;

	nodes_[parentNode].TopLeftChild     = NewNode_();
	nodes_[parentNode].TopRightChild    = NewNode_();
	nodes_[parentNode].BottomLeftChild  = NewNode_();
	nodes_[parentNode].BottomRightChild = NewNode_();

	BoundNode_ (nodes_[parentNode].TopLeftChild, x1, y1, middleX, middleY);
	BoundNode_ (nodes_[parentNode].TopRightChild, middleX, y1, x2, middleY);
	BoundNode_ (nodes_[parentNode].BottomLeftChild, x1, middleY, middleX, y2);
	BoundNode_ (nodes_[parentNode].BottomRightChild, middleX, middleY, x2, y2);

	if (RenderFacesCount (x1, y1, middleX, middleY, maxFacesInNode_) >= maxFacesInNode_) Subdivide_ (nodes_[parentNode].TopLeftChild);
	else MakeLeaf_ (nodes_[parentNode].TopLeftChild);

	if (RenderFacesCount (middleX, y1, x2, middleY, maxFacesInNode_) >= maxFacesInNode_) Subdivide_ (nodes_[parentNode].TopRightChild);
	else MakeLeaf_ (nodes_[parentNode].TopRightChild);

	if (RenderFacesCount (x1, middleY, middleX, y2, maxFacesInNode_) >= maxFacesInNode_) Subdivide_ (nodes_[parentNode].BottomLeftChild);
	else MakeLeaf_ (nodes_[parentNode].BottomLeftChild);

	if (RenderFacesCount (middleX, middleY, x2, y2, maxFacesInNode_) >= maxFacesInNode_) Subdivide_ (nodes_[parentNode].BottomRightChild);
	else MakeLeaf_ (nodes_[parentNode].BottomRightChild);
}


UInt16 IslandQuadtreeBuilder::NewNode_()
{
	if (nodeCount_ >= USHRT_MAX - 2) _throw ("Can't allocate a new node. Too many nodes.");
	return nodeCount_++;
}


void IslandQuadtreeBuilder::MakeLeaf_ (UInt16 node)
{
	nodes_[node].BottomLeftChild  = 0;
	nodes_[node].BottomRightChild = 0;
	nodes_[node].TopLeftChild     = 0;
	nodes_[node].TopRightChild    = 0;
}


void IslandQuadtreeBuilder::BoundNode_ (UInt16 node, UInt16 x1, UInt16 y1, UInt16 x2, UInt16 y2)
{
	nodes_[node].X1 = x1;
	nodes_[node].X2 = x2;
	nodes_[node].Y1 = y1;
	nodes_[node].Y2 = y2;
}
