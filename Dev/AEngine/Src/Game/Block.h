
//=================================================================================================
//=== AEngine  is a part of  Archipelago  by  SSGL Gamedev Studio Copyright (C) 2012-2014  ========
//=================================================================================================
//
// This file is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// This file is managed by 
// - Leonid Stolyarov [lndstol]     lndstol@gmail.com
//
//=================================================================================================

#ifndef _BLOCK_H_
#define _BLOCK_H_

#include <cstdlib>
#include <cstring>
#include <typeinfo>
#include <type_traits>

#include "../BuildOptions.h"
#include "AEngineDefinitions.h"

namespace AEngine {
namespace GameEntities {
//=================================================================================================



//=================================================================================================
} }

#endif
