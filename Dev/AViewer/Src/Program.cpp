
#include <stdio.h>
#include <stdlib.h>

#include "../../AEngine/Src/Core/Array.hpp"

using namespace AEngine;
using namespace AEngine::Generics;

int main(int argc, char **argv)
{
    StaticArray<Int, 100> a;
    a[0] = 5;
    printf("%d\n", a[0]);

    HeapArray<Int> b(100000);
    printf("%d\n", b.Size());

    printf("Hello\n");
    system("pause");
    return 0;
}
